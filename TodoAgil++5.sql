/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     04-11-2014 23:59:47                          */
/*==============================================================*/


drop table if exists account;

drop table if exists account_type;

drop table if exists activity;

drop table if exists activity_type;

drop table if exists asignature;

drop table if exists asignature_detail;

drop table if exists card;

drop table if exists card_status;

drop table if exists dash;

drop table if exists groupp;

drop table if exists list;

drop table if exists member;

drop table if exists project;

drop table if exists role;

drop table if exists sprint;

drop table if exists sprint_group_detail;

drop table if exists team;

drop table if exists user;

drop table if exists user_card_detail;

drop table if exists user_project_detail;

drop table if exists user_rol;

drop table if exists user_sprint_group;

/*==============================================================*/
/* Table: account                                               */
/*==============================================================*/
create table account
(
   account_id           int not null auto_increment,
   account_type_id      int not null,
   user_id              int not null,
   account_user_name    varchar(50) not null,
   account_email        varchar(100) not null,
   account_key          varchar(70) default NULL,
   account_secret       varchar(70) default NULL,
   account_token        varchar(70) default NULL,
   primary key (account_id)
);

/*==============================================================*/
/* Table: account_type                                          */
/*==============================================================*/
create table account_type
(
   account_type_id      int not null auto_increment,
   account_type_name    varchar(50) not null,
   primary key (account_type_id)
);

/*==============================================================*/
/* Table: activity                                              */
/*==============================================================*/
create table activity
(
   activity_id          int not null auto_increment,
   external_activity_id varchar(100) not null,
   card_id              int default NULL,
   member_id            int not null,
   activity_type_id     int not null,
   activity_date        datetime not null,
   primary key (activity_id)
);

/*==============================================================*/
/* Table: activity_type                                         */
/*==============================================================*/
create table activity_type
(
   activity_type_id     int not null auto_increment,
   account_type_id      int not null,
   activity_type_name   varchar(50) not null,
   primary key (activity_type_id)
);

/*==============================================================*/
/* Table: asignature                                            */
/*==============================================================*/
create table asignature
(
   asignature_id        int not null auto_increment,
   asignature_name      varchar(50) character set utf8 not null,
   asignature_theory_hours int not null,
   asignature_exercises_hours int not null,
   asignature_lab_hours int not null,
   asignature_semester  int not null,
   asignature_year      int not null,
   asignature_close     int not null,
   primary key (asignature_id)
);

/*==============================================================*/
/* Table: asignature_detail                                     */
/*==============================================================*/
create table asignature_detail
(
   asignature_detail_id int not null auto_increment,
   asignature_id        int default NULL,
   user_id              int default NULL,
   primary key (asignature_detail_id)
);

/*==============================================================*/
/* Table: card                                                  */
/*==============================================================*/
create table card
(
   card_id              int not null auto_increment,
   ext_card_id          varchar(100) not null,
   dash_id              int not null,
   list_id              int not null,
   card_status_id       int not null,
   card_name            varchar(100) not null,
   card_deadline        datetime default NULL,
   card_start_date      datetime default NULL,
   card_end_date        datetime default NULL,
   card_creation_date   datetime default NULL,
   primary key (card_id)
);

/*==============================================================*/
/* Table: card_status                                           */
/*==============================================================*/
create table card_status
(
   card_status_id       int not null auto_increment,
   card_status_name     varchar(20) not null,
   primary key (card_status_id)
);

/*==============================================================*/
/* Table: dash                                                  */
/*==============================================================*/
create table dash
(
   dash_id              int not null auto_increment,
   groupp_id            int,
   external_dash_id     varchar(50) not null,
   dash_name            varchar(100) not null,
   dash_url             varchar(50) not null,
   primary key (dash_id)
);

/*==============================================================*/
/* Table: groupp                                                */
/*==============================================================*/
create table groupp
(
   groupp_id            int not null auto_increment,
   groupp_name          varchar(50) not null,
   groupp_technical_objectives varchar(100) default NULL,
   groupp_state         int,
   primary key (groupp_id)
);

/*==============================================================*/
/* Table: list                                                  */
/*==============================================================*/
create table list
(
   list_id              int not null auto_increment,
   ext_list_id          varchar(100) not null,
   dash_id              int not null,
   list_name            varchar(100) not null,
   position             int not null,
   primary key (list_id)
);

/*==============================================================*/
/* Table: member                                                */
/*==============================================================*/
create table member
(
   member_id            int not null auto_increment,
   dash_id              int not null,
   account_id           int default NULL,
   external_member_id   varchar(100) not null,
   user_member_name     varchar(100) not null,
   primary key (member_id)
);

/*==============================================================*/
/* Table: project                                               */
/*==============================================================*/
create table project
(
   project_id           int not null auto_increment,
   asignature_id        int,
   project_name         varchar(40) not null,
   project_description  varchar(120),
   project_product_owner varchar(40) not null,
   primary key (project_id)
);

/*==============================================================*/
/* Table: role                                                  */
/*==============================================================*/
create table role
(
   id_role              int not null auto_increment,
   role_user            varchar(50),
   role_name            varchar(50),
   primary key (id_role)
);

/*==============================================================*/
/* Table: sprint                                                */
/*==============================================================*/
create table sprint
(
   sprint_id            int not null auto_increment,
   project_id           int,
   sprint_name          varchar(50) not null,
   sprint_description   varchar(200) default NULL,
   sprint_start_date    datetime not null,
   sprint_end_date      datetime not null,
   sprint_state         varchar(40),
   primary key (sprint_id)
);

/*==============================================================*/
/* Table: sprint_group_detail                                   */
/*==============================================================*/
create table sprint_group_detail
(
   sprint_group_detail_id int not null auto_increment,
   sprint_id            int,
   groupp_id            int,
   sprint_group_detail_objective varchar(120),
   primary key (sprint_group_detail_id)
);

/*==============================================================*/
/* Table: team                                                  */
/*==============================================================*/
create table team
(
   team_id              int not null auto_increment,
   account_id           int not null,
   dash_id              int not null,
   primary key (team_id)
);

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   user_id              int not null auto_increment,
   user_rol_id          int not null,
   user_name            varchar(200) not null,
   user_email           varchar(100) not null,
   user_login           varchar(30) not null,
   user_pass            varchar(200) not null,
   primary key (user_id)
);

/*==============================================================*/
/* Table: user_card_detail                                      */
/*==============================================================*/
create table user_card_detail
(
   user_card_detail_id  int not null auto_increment,
   member_id            int not null,
   card_id              int not null,
   primary key (user_card_detail_id)
);

/*==============================================================*/
/* Table: user_project_detail                                   */
/*==============================================================*/
create table user_project_detail
(
   user_project_detail_id int not null auto_increment,
   project_id           int,
   user_id              int,
   primary key (user_project_detail_id)
);

/*==============================================================*/
/* Table: user_rol                                              */
/*==============================================================*/
create table user_rol
(
   user_rol_id          int not null auto_increment,
   user_rol_name        varchar(50) not null,
   user_rol_permission  varchar(200) not null,
   primary key (user_rol_id)
);

/*==============================================================*/
/* Table: user_sprint_group                                     */
/*==============================================================*/
create table user_sprint_group
(
   user_user_id         int not null,
   user_sprint_group_id int not null auto_increment,
   groupp_id            int,
   user_sprint_group_smaster tinyint not null,
   primary key (user_sprint_group_id)
);

alter table account add constraint fk_fk_fk_es_de foreign key (account_type_id)
      references account_type (account_type_id) on delete restrict on update restrict;

alter table account add constraint fk_fk_fk_tiene_asociada foreign key (user_id)
      references user (user_id) on delete restrict on update restrict;

alter table activity add constraint fk_fk_fk_contiene foreign key (card_id)
      references card (card_id) on delete restrict on update restrict;

alter table activity add constraint fk_fk_fk_ejecuta foreign key (member_id)
      references member (member_id) on delete restrict on update restrict;

alter table activity add constraint fk_fk_fk_tipo_de foreign key (activity_type_id)
      references activity_type (activity_type_id) on delete restrict on update restrict;

alter table activity_type add constraint fk_fk_fk_perteneces_a_un foreign key (account_type_id)
      references account_type (account_type_id) on delete restrict on update restrict;

alter table asignature_detail add constraint fk_fk_relationship_22 foreign key (asignature_id)
      references asignature (asignature_id) on delete restrict on update restrict;

alter table asignature_detail add constraint fk_fk_relationship_23 foreign key (user_id)
      references user (user_id) on delete restrict on update restrict;

alter table card add constraint fk_fk_fk_posee foreign key (list_id)
      references list (list_id) on delete restrict on update restrict;

alter table card add constraint fk_fk_fk_se_encuentra foreign key (card_status_id)
      references card_status (card_status_id) on delete restrict on update restrict;

alter table card add constraint fk_fk_fk_tiene foreign key (dash_id)
      references dash (dash_id) on delete restrict on update restrict;

alter table dash add constraint fk_fk_reference_32 foreign key (groupp_id)
      references groupp (groupp_id) on delete restrict on update restrict;

alter table list add constraint fk_fk_fk_cuenta foreign key (dash_id)
      references dash (dash_id) on delete restrict on update restrict;

alter table member add constraint fk_fk_fk_cuenta_con foreign key (dash_id)
      references dash (dash_id) on delete restrict on update restrict;

alter table member add constraint fk_fk_fk_puede_estar foreign key (account_id)
      references account (account_id) on delete restrict on update restrict;

alter table project add constraint fk_relationship_28 foreign key (asignature_id)
      references asignature (asignature_id) on delete restrict on update restrict;

alter table sprint add constraint fk_relationship_29 foreign key (project_id)
      references project (project_id) on delete restrict on update restrict;

alter table sprint_group_detail add constraint fk_fk_reference_29 foreign key (groupp_id)
      references groupp (groupp_id) on delete restrict on update restrict;

alter table sprint_group_detail add constraint fk_fk_reference_30 foreign key (sprint_id)
      references sprint (sprint_id) on delete restrict on update restrict;

alter table team add constraint fk_fk_fk_puede_tener foreign key (account_id)
      references account (account_id) on delete restrict on update restrict;

alter table team add constraint fk_fk_fk_trabaja foreign key (dash_id)
      references dash (dash_id) on delete restrict on update restrict;

alter table user add constraint fk_fk_fk_actua foreign key (user_rol_id)
      references user_rol (user_rol_id) on delete restrict on update restrict;

alter table user_card_detail add constraint fk_fk_fk_existe foreign key (card_id)
      references card (card_id) on delete restrict on update restrict;

alter table user_card_detail add constraint fk_fk_fk_se_asigna foreign key (member_id)
      references member (member_id) on delete restrict on update restrict;

alter table user_project_detail add constraint fk_fk_reference_27 foreign key (project_id)
      references project (project_id) on delete restrict on update restrict;

alter table user_project_detail add constraint fk_fk_reference_28 foreign key (user_id)
      references user (user_id) on delete restrict on update restrict;

alter table user_sprint_group add constraint fk_fk_reference_31 foreign key (groupp_id)
      references groupp (groupp_id) on delete restrict on update restrict;

alter table user_sprint_group add constraint fk_fk_sprint_groupp_has_user_user1 foreign key (user_user_id)
      references user (user_id) on delete restrict on update restrict;

